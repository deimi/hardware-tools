// Bobbin holder for fly tying

include <../../libs/BOSL/transforms.scad>;
include <../../libs/BOSL/shapes.scad>;

$fn = 50;

difference() {
  union() {
    difference() {
      import("./bobbin_holder_2.stl");
      move([ -7, 30, 4 ])
          cuboid([ 7, 14.5, 8 ], chamfer = 0.5, trimcorners = true);
    }
    move([ -7, 26.2, 4 ])
        cuboid([ 7, 7, 8 ], chamfer = 0.5, trimcorners = true);
  }
  move([ -7, 20.7, 4.5 ])
      xrot(-90)
          cyl(l = 8, d = 5, chamfer = 1.5, chamfang = 37);
  move([ -7, 22, 4.5 ])
      xrot(-90)
          cylinder(10, d = 4.7);
}

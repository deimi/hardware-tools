// clang-format off
use <../../libs/MCAD/boxes.scad>;
use <../../libs/MCAD/multiply.scad>;
include <../../libs/NopSCAD/lib.scad>;
// clang-format on

Plate_Thickness = 6;
2D_Top_View = false;    // for being able to export the top view to a svg file
Show_Plate = true;
Show_Router = false;
Show_Ruler = true;
Ruler_depth = 0;

module __Customizer_Limit__() {
}
$fn = 50;
Plate_Lenght = 255;
Plate_Widht = 140;

// *********utilities*********
module mirror_copy(vec) {
  children();
  mirror(vec) children();
}

module translate_copy(vec) {
  children();
  translate(vec) children();
}

module show_router() {
  color("gray")
      translate([ 0, 0, -100 ])
          cylinder(d = 88, h = 200, center = true);
}

module cs_screw_hole(type, sink = 1) {
  translate([ 0, 0, -sink ]) {
    cylinder(r = screw_head_radius(type), h = sink);
    screw(type, length = Plate_Thickness);
    translate([ 0, 0, -Plate_Thickness ])
        cylinder(r = screw_clearance_radius(type), h = Plate_Thickness);
  }
}

module ruler_marks(length, depth = 1) {
  10_mm(depth);
  for (i = [1:length]) {
    translate([ 0, i, 0 ]) 1_mm(depth);
    if (i % 5 == 0) {
      translate([ 0, i, 0 ]) 5_mm(depth);
    }
    if (i % 10 == 0) {
      translate([ 0, i, 0 ]) 10_mm(depth);
    }
  }

  mark_thickness = 0.25;
  module 1_mm(depth) {
    translate([ 0, -(mark_thickness / 2), 0 ]) {
      if (depth != 0)
        cube([ 7, mark_thickness, depth ]);
      else
        square([ 7, mark_thickness ]);
    }
  }
  module 5_mm(depth) {
    translate([ 0, -(mark_thickness / 2), 0 ]) {
      if (depth != 0)
        cube([ 10, mark_thickness, depth ]);
      else
        square([ 10, mark_thickness ]);
    }
  }
  module 10_mm(depth) {
    translate([ 0, -(mark_thickness / 2), 0 ]) {
      if (depth != 0)
        cube([ 15, mark_thickness, depth ]);
      else
        square([ 15, mark_thickness ]);
    }
  }
}

// *********actual object*********
module rounded_base_plate() {
  translate([ 0, 0, Plate_Thickness / 2 ])
      roundedCube([ Plate_Lenght, Plate_Widht, Plate_Thickness ], r = 5, sidesonly = true, center = true);
}

module drill_bit_hole() {
  cylinder(Plate_Thickness, d = 36);
  translate([ 0, 4, 0 ])
      cylinder(Plate_Thickness, d = 30);
}

module table_mounting_holes() {
  mirror_copy([ 0, 1, 0 ]) mirror_copy([ 1, 0, 0 ])
      translate([ (Plate_Lenght / 2) - 15, (Plate_Widht / 2) - 15, 0 ])
          translate([ 0, 0, Plate_Thickness ])
              cs_screw_hole(M5_cs_cap_screw);

  // inner group of screws to prevent potential vibration in the middle of the plate
  mirror_copy([ 0, 1, 0 ]) mirror_copy([ 1, 0, 0 ])
      translate([ 90 / 2, 70 / 2, 0 ])
          translate([ 0, 0, Plate_Thickness ])
              cs_screw_hole(M5_cs_cap_screw);
}

module router_mounting_holes() {
  translate([ 0, 4, 0 ]) {
    mirror_copy([ 1, 0, 0 ])
        translate([ 56 / 2, 45 / 2, 0 ])
            translate([ 0, 0, Plate_Thickness ])
                cs_screw_hole(M4_cs_cap_screw);
    mirror_copy([ 1, 0, 0 ])
        translate([ 54.3 / 2, -45 / 2, 0 ])
            translate([ 0, 0, Plate_Thickness ])
                cs_screw_hole(M4_cs_cap_screw);
  }
}

module trimmer_guide_hole() {
  translate([ 0, -42.5, Plate_Thickness / 2 ])
      roundedCube([ 34, 15, Plate_Thickness ], 4, true, center = true);
}

module ruler() {
  Ruler_position = (Plate_Lenght / 2) - 42.5;
  mirror_copy([ 1, 0, 0 ])
      mirror_copy([ 0, 1, 0 ])
          translate([ Ruler_position, 0, Plate_Thickness - Ruler_depth ])
              ruler_marks(60, depth = Ruler_depth);

  module ruler_text() {
    Number_Strings = [ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" ];
    Number_Height = 5;
    for (i = [0:6]) {
      translate([ 0, i * 10, 0 ]) {
        text(Number_Strings[i], size = Number_Height, valign = "center", halign = "center", script = "latin");    // We need to explicitly set the script parameter, else we
                                                                                                                  // get an error when importing the file into freecad 0.19.1
                                                                                                                  // see https://forum.freecadweb.org/viewtopic.php?f=3&t=58897
      }
      translate([ 0, -i * 10, 0 ]) {
        text(Number_Strings[i], size = Number_Height, valign = "center", halign = "center", script = "latin");
      }
    }
  }

  Number_Depth = Ruler_depth;
  Number_Offset_To_Ruler = 18;
  translate_copy([ -2 * Ruler_position - 2 * Number_Offset_To_Ruler, 0, 0 ]) {
    translate([ Ruler_position + Number_Offset_To_Ruler, 0, Plate_Thickness - Number_Depth ]) {
      if (Number_Depth != 0)
        linear_extrude(Number_Depth)
            ruler_text();
      else
        ruler_text();
    }
  }
}

module complete_table_plate() {
  difference() {
    if (Show_Plate)
      difference() {
        rounded_base_plate();
        drill_bit_hole();
        table_mounting_holes();
        router_mounting_holes();
        trimmer_guide_hole();
      }
    if (Show_Ruler)
      ruler();
  }
}

if (!2D_Top_View)
  complete_table_plate();
else
  projection(cut = false) complete_table_plate();

if (Show_Router)
  show_router();

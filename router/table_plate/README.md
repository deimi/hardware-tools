This is a table mounting plate for a Makita RT700C router. Basically it's like the ones which are sold on Amazon, AliExpress, etc. I designed it by my own to be able to mill it out of a hard plastic board with my CNC machine.

![preview](3d_preview.png)

# Specs
- Dimension: 255mm x 140mm
- Bolts  
Table mount: M5 countersink  
Router mount: M4 countersink (original from Makita)

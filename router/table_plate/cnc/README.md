This directory contains a Freecad object for generating Snapmaker CNC jobs out of the Openscad file.

# How to convert Openscad to Freecad solid

Follow these steps to be able to create Freecad CNC jobs out of an Openscad project.

1. Import your Openscad file into your Freecad project  
Hint: you need the Openscad plugin to be set up
1. Find the object of highest hierarchy which represents your actual object
1. Convert it to a solid  
Part -> Convert to solid
1. You can delete now all the imported object as you will only need the solid object anymore
1. Refine the shape of your object  
Part -> Create a copy -> Refine shape
1. The refined shape object can now be used like any other Freecad created object for generating path jobs


*Note: The Openscad file will not be synced to the Freecad project. This means that if you change the Openscad file, you need to reimport the*
*Openscad file and repeat the steps 1-4. Instead of doing step 5 (refine shape) you just change your refine object to use another base object then.*  
*That way it all your path jobs will automatically update as they are linked to the refined object.*

## Add 2D ruler for v-carving

You need to add the ruler in a separate step to Freecad, as it needs to be a 2D object for engraving. See https://wiki.freecadweb.org/Path_Vcarve

1. Export the ruler in Openscad to an svg file
1. Import the svg file to Freecad and select SVG as geometry
1. Select all imported files
1. Draft - Modification - Upgrade
1. Draft - Modification - Downgrade
1. Part - Compound
1. Create VCarve Job

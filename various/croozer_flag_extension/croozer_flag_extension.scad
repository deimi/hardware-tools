// the extension part for putting together the two poles of the croozer flag

$fn = 50;

linear_extrude(height = 80) {
  difference() {
    circle(d = 15);
    circle(d = 6.2);
  }
}

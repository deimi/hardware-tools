// Female part of a festool 36mm connector

// clang-format off
include <../libs/BOSL/transforms.scad>;
use <../libs/dotSCAD/src/bend_extrude.scad>;
// clang-format on

_tightness_scale = 1.02;

function festool_female_height() = 40;
function festool_female_outer_diameter() = 46 * _tightness_scale;
function festool_female_inner_diameter() = 36.2 * _tightness_scale;
function festool_female_connection_diameter() = 30 * _tightness_scale;
function festool_female_wall_thickness() = 4;

module __Customizer_Limit__() {
}
$fn = 50;
preview_render_ = 0.01;
arrow_depth = 1;
arrow_height = 20;
function _arrow_text() = "\u21B2";    // left
// function _arrow_text() = "\u21B3";  // right

module festool_female(with_arrows = false) {
  if (with_arrows) {
    difference() {
      _base_festool_female();
      zmove(10)
          _3d_arrows();
    }
  } else {
    _base_festool_female();
  }
}

module _2d_arrow() {
  back(0.4) left(4.7)
      text(_arrow_text(), size = arrow_height, font = "DejaVu Sans");
}

module _3d_arrow() {
  bend_extrude(size = [ 13, 21 ], thickness = arrow_depth, angle = 31, frags = 10)
      _2d_arrow();
}

module _3d_arrows() {
  zrot_copies([ 0, 120, 240 ])
      zrot(-65)
          _3d_arrow();
}

module _base_festool_female() {
  scale([ _tightness_scale, _tightness_scale, 1 ])
      import("./festool_female.stl");
}

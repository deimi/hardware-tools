// Adapter for connecting my cyclone hose to any tool with a different diameter

Adapter_inner_diameter = 39;
Adapter_wall_thickness = 3;

module __Customizer_Limit__() {
}
$fn = 50;
Adapter_vacuum_side_outer_diameter = 30.5;

module inner_hull() {
  cylinder(30, d = Adapter_inner_diameter);
  translate([ 0, 0, 30 ])
      cylinder(15, d1 = Adapter_inner_diameter, d2 = Adapter_vacuum_side_outer_diameter - Adapter_wall_thickness);
  translate([ 0, 0, 30 + 15 ])
      cylinder(30, d = Adapter_vacuum_side_outer_diameter - Adapter_wall_thickness);
}

difference() {
  resize([ Adapter_inner_diameter + Adapter_wall_thickness, Adapter_inner_diameter + Adapter_wall_thickness, 0 ])
      inner_hull();
  inner_hull();
}

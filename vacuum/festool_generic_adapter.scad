// Adapter for connecting my festool hose to my floor nozzle

// clang-format off
include <../libs/BOSL/transforms.scad>;
include <./festool_female.scad>;
// clang-format on

// Outer diameter of the non festool part
Adapter_outer_diameter = 35.1;
// Inner diameter of the non festool part
Adapter_inner_diameter = 27.1;
// Length of the non festool part
Adapter_length = 40;
// Length of the transition between the festool part and the non festool part
Transition_length = 10;
// At what length shall the inner edge of the adapter be rounded
Inner_edge_rounding_length = 3;
// Arrows on the festool part for direction indication?
Direction_arrows = false;

module __Customizer_Limit__() {
}
$fn = 50;
preview_render_ = 0.01;
Adapter_wall_thickness = (Adapter_outer_diameter - Adapter_inner_diameter) / 2;
Outer_edge_rounding_radius = 1;

main();

module main() {
  check_input();
  festool_female(with_arrows = Direction_arrows);
  adapter();
}

module check_input() {
  echo(str("Adapter wall thickness is ", Adapter_wall_thickness, "mm"));
  assert(Adapter_outer_diameter > Adapter_inner_diameter, "Outer adapter diameter must be bigger then inner one");
  assert(Adapter_outer_diameter > 0, "Outer adapter diameter must be positive");
  assert(Adapter_inner_diameter > 0, "Inner adapter diameter must be positive");
  assert(Adapter_wall_thickness >= 2, "Adapter wall thickness must be bigger then 1mm");
  assert(Adapter_length > 0, "Adapter length must be positive");
  assert(Transition_length > 0, "Transition length must be positive");
}

module transition() {
  difference() {
    down(Transition_length)
        cylinder(Transition_length, d1 = Adapter_outer_diameter, d2 = festool_female_outer_diameter());
    down(Transition_length + preview_render_)
        cylinder(Transition_length + preview_render_ * 2, d1 = Adapter_inner_diameter, d2 = festool_female_connection_diameter());
  }
}

module adapter() {
  transition();
  down(Adapter_length) {
    difference() {
      linear_extrude(Adapter_length - Transition_length) {
        difference() {
          circle(d = Adapter_outer_diameter);
          circle(d = Adapter_inner_diameter);
        }
      }
      union() {
        // inner edge rounding
        down(preview_render_)
            cylinder(Inner_edge_rounding_length, d1 = Adapter_inner_diameter + 2, d2 = Adapter_inner_diameter);
        // outer edge rounding
        rotate_extrude() {
          xrot(-180, [ Adapter_outer_diameter / 2 - Outer_edge_rounding_radius / 2, Outer_edge_rounding_radius / 2, 0 ]) {
            xmove(Adapter_outer_diameter / 2 - Outer_edge_rounding_radius) {
              difference() {
                square(Outer_edge_rounding_radius, center = false);
                intersection() {
                  square(Outer_edge_rounding_radius, center = false);
                  circle(r = Outer_edge_rounding_radius);
                }
              }
            }
          }
        }
      }
    }
  }
}
